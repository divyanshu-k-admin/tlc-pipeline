from urllib.request import urlretrieve, urlopen
import urllib.request
import datetime as dt
import requests
from typing import Any
from dateutil.relativedelta import relativedelta
import os
import pandas as pd


def import_data(end_date: str = None, start_date: Any = dt.datetime.today().date()) -> None:
    # The
    """
    :param start_date: start date is the date at lest 3 years before the latest date
    :param end_date: end date is the latest date for which the data is available
    We want to generalise this in such a way that it takes the latest date as end date and based on that assigns the
    start date as : end date - 3 years
    """
    if type(start_date) == str:
        start_date = dt.datetime.strptime(start_date, "%Y-%m").date()

    if end_date is not None:
        end = dt.datetime.strptime(end_date, "%Y-%m").date()
    else:
        end = start_date - relativedelta(years=3)  # if no argument is provided while calling the function, it will
        # automatically take the end date as start_date - 3 years
    # The first four lines below are the urls corresponding to the taxi types
    yellow_trip = "https://s3.amazonaws.com/nyc-tlc/trip+data/yellow_tripdata_"
    green_trip = "https://s3.amazonaws.com/nyc-tlc/trip+data/green_tripdata_"
    fhv_trip = "https://s3.amazonaws.com/nyc-tlc/trip+data/fhv_tripdata_"
    fhvhv_trip = "https://s3.amazonaws.com/nyc-tlc/trip+data/fhvhv_tripdata_"
    month_delta = relativedelta(months=1)
    taxi_category = ['yellow', 'green', 'fhv', 'fhvhv']
    # Now we want to check if the url exists or not. If the request status code returns > 400 then it's a client error
    # and url doesn't exist nd when this happens, the latest date is updated until the data is found
    while requests.get(f"{yellow_trip}{start_date.strftime('%Y-%m')}.csv").status_code > 400 and requests.get(f"{green_trip}{start_date.strftime('%Y-%m')}.csv").status_code > 400 and requests.get(f"{fhv_trip}{start_date.strftime('%Y-%m')}.csv").status_code > 400 and requests.get(f"{fhvhv_trip}{start_date.strftime('%Y-%m')}.csv").status_code > 400:
        start_date -= month_delta

    # The data extraction part begins from here (this process takes some time because the data is huge compared to
    # what local systems can handle)
    while start_date >= end:
        date = start_date.strftime("%Y-%m")
        year = start_date.strftime("%Y")
        for i in taxi_category:
            print(f"Fetching data for {i} taxi dated {date}")
            url = f"https://s3.amazonaws.com/nyc-tlc/trip+data/{i}_tripdata_{date}.csv"
            try:
                urlopen(url)
                path = f"imported_data/{i}_taxi/{year}"
                parquet_path = f"parquet_data/{i}_taxi/{year}"
                if not os.path.exists(path):  # structuring the data into files and directories so as to avoid chaos
                    os.makedirs(path)
                if not os.path.exists(f"{path}/data_{i}_taxi_{date}.csv"):
                    urlretrieve(url, f'{path}/data_{i}_taxi_{date}.csv')
                    print(f"Downloaded {i}_taxi {date}")
                else:
                    print(f"Data already exists for {i} taxi dated {date}")
                    pass
                # The data extraction part ends here

                # I have included the data conversion into parquet format here so that as the files are imported
                # they are also automatically converted to parquet for my colleagues to use
                if not os.path.exists(parquet_path):
                    os.makedirs(parquet_path)
                if not os.path.exists(f'{parquet_path}/data_{i}_taxi_{date}.parquet'):
                    df = pd.read_csv(f'{path}/data_{i}_taxi_{date}.csv')
                    df.to_parquet(f'{parquet_path}/data_{i}_taxi_{date}.parquet')
                    print('Converted to parquet format')
                else:
                    print('Data already exists in parquet format')
                    pass
            except urllib.request.HTTPError:
                print(f"Data not found for {i} taxi for {date}")

        start_date -= month_delta

    print("DATA IMPORTED SUCCESSFULLY")
