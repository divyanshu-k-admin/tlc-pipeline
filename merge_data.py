import os
import pandas as pd
import numpy as np


def merge(path_yellow: str = f"imported_data/yellow_taxi", path_green: str = f"imported_data/green_taxi",
          merged_path: str = f"merged_data") -> None:
    """
    :param path_yellow: The path to which all the yellow_taxi data is imported
    :param path_green: The path to which all the green taxi data is imported
    :param merged_path: The path where all the merged data will be stored
    :return:
    """
    # The idea is that we access the directories where we have imported the data for yellow and green taxis and
    # then access the data for the same month and year in both taxis and merge them while storing it in a separate
    # directory which we call merged_data
    file_yellow = os.listdir(path_yellow)
    for i in file_yellow:
        files = os.listdir(f"{path_yellow}/{i}")
        files_ = os.listdir(f"{path_green}/{i}")
        for j, _ in enumerate(files):
            df_yellow = pd.read_csv(f"{path_yellow}/{i}/{files[j]}")
            df_yellow["taxi_category"] = ['yellow'] * len(df_yellow)
            # we have to rename the columns "tpep_...." and "lpep_...." because they correspond to the same thing
            df_yellow = df_yellow.rename(columns={"tpep_pickup_datetime": "pickup_datetime",
                                                  "tpep_dropoff_datetime": "dropoff_datetime"})
            df_green = pd.read_csv(f"{path_green}/{i}/{files_[j]}")
            df_green["taxi_category"] = ['green'] * len(df_green)
            df_green = df_green.rename(columns={"lpep_pickup_datetime": "pickup_datetime",
                                                "lpep_dropoff_datetime": "dropoff_datetime"})
            merge_df = pd.concat([df_yellow, df_green], ignore_index=True)
            # from this point we will add some more columns to the data so as to have more atomicity for date and time
            merge_df["pickup_datetime"] = pd.to_datetime(merge_df["pickup_datetime"])
            merge_df["dropoff_datetime"] = pd.to_datetime(merge_df["dropoff_datetime"])
            merge_df["pickup_day"] = merge_df["pickup_datetime"].dt.day_name()
            merge_df["dropoff_day"] = merge_df["dropoff_datetime"].dt.day_name()
            merge_df["pickup_hour_of_day"] = merge_df["pickup_datetime"].dt.time
            merge_df["dropoff_hour_of_day"] = merge_df["dropoff_datetime"].dt.time
            merge_df["year"] = merge_df["pickup_datetime"].dt.year
            merge_df["hour"] = merge_df["pickup_datetime"].dt.hour
            merge_df["trip_duration_hr"] = (merge_df["dropoff_datetime"] - merge_df["pickup_datetime"])/np.timedelta64(1,'h')
            if not os.path.exists(f"{merged_path}/{i}"):
                os.makedirs(f"{merged_path}/{i}")
            if not os.path.exists(f"{merged_path}/{i}/merged_data_{files[j][17:-4]}.csv"):
                merge_df.to_csv(f"{merged_path}/{i}/merged_data_{files[j][17:-4]}.csv")
            else:
                print("Merged data exists")
