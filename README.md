### The documentation to this pipeline can be found in the Documentation.txt within this repository.

## Instructions on how to run the pipeline:
- clone the repository to your local system.  
- Open the command prompt after navigating to the repository. The first command it takes is "pip install -r requirements.txt".  
- After all the necessary packages are installed, open a python shell and run "from import_data import import_data".  
- Next, call the function import_data(). This will start importing the data from the web to the repository. For more information on the function arguments look into the documentation step 1.  
- After the data is successfully imported, run "from merge_data import merge".  
- Next, call the function merge(). This will merge the imported data for yellow and green taxis. For more information on the arguments of the function look into the documentation step 3.  
- When the process is finished, you can open the queries.ipynb notebook and run the queries in it.  

NOTE: I tried dong this using docker but it gave me certain memory issues due to which the container would exit everytime, and I did not have much time to work on it. 